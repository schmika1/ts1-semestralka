package cz.fel.cvut.pjv;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Provides methods for drawing images and text on the screen using the Java Graphics object.
 */
public class Painter {
    private Graphics g;

    /**
     * Creates a new Painter object with the specified Graphics object.
     *
     * @param g the Graphics object used for drawing
     */
    public Painter(Graphics g) {
        this.g = g;
    }

    /**
     * Draws an image on the screen at the specified position.
     *
     * @param image the image to be drawn
     * @param x     the x-coordinate of the position to draw the image
     * @param y     the y-coordinate of the position to draw the image
     */
    public void drawImage(BufferedImage image, int x, int y) {
        g.drawImage(image, x, y, null);
    }

    /**
     * Draws a string of text on the screen at the specified position.
     *
     * @param text the text to be drawn
     * @param x    the x-coordinate of the position to draw the text
     * @param y    the y-coordinate of the position to draw the text
     */
    public void drawString(String text, int x, int y) {
        g.setColor(Color.WHITE);
        Font font = new Font("Arial", Font.BOLD, 12);
        g.setFont(font);
        g.drawString(text, x, y);
    }

    /**
     * Returns the Graphics object used for drawing.
     *
     * @return the Graphics object
     */
    public Graphics getGraphics() {
        return g;
    }
}
