package cz.fel.cvut.pjv;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Class representing a player's inventory.
 */
public class Inventory {
    private int maxBombs;
    private int lives;
    private int bombs;
    private static final GameLogger logger = GameLogger.getInstance();
    private List<Gun> guns;

    /**
     * Default constructor used for setting Player's Inventory and reading it from a file.
     * It reads the inventory from the "inventory.txt" file and sets the values accordingly.
     */
    public Inventory() {
        this.guns = new ArrayList<>();
        readInventory();
    }

    /**
     * Constructor for the Inventory class used for enemies.
     *
     * @param maxBombs  maximum number of bombs
     * @param bombs     number of bombs in inventory
     * @param lives     number of lives in inventory
     */
    public Inventory( int maxBombs, int bombs, int lives) {
        this.maxBombs = maxBombs;
        this.bombs = bombs;
        this.lives = lives;
        this.guns = new ArrayList<>();
    }

    /**
     * Adds an item to the player's inventory after picking it up.
     *
     * @param item the item to be added
     */
    public void addItem(GameObject item) {
        switch (item.getObjectType()) {
            case "Life":
                this.lives += 1;
                break;
            case "BombBonus":
                this.maxBombs += 1;
                this.bombs += 1;
                break;
            case "Gun":
                this.guns.add((Gun) item);
                break;
        }
        logger.log(Level.INFO, "Item added to inventory: " + item.getObjectType());
    }

    /**
     * Reads the inventory data from a file named "inventory.txt".
     * If the file does not exist or is empty, the default inventory values are used.
     */
    public void readInventory() {
        try {
            File inventoryFile = new File("inventory.txt");
            if (!inventoryFile.exists()) {
                inventoryFile.createNewFile();
                setDefaultInventory();
                logger.log(Level.WARNING, "No inventory file was found. Using default inventory.");
                return;
            }
            BufferedReader reader = new BufferedReader(new FileReader(inventoryFile));
            String line = reader.readLine();
            List<String> items = new ArrayList<>();
            while (line != null) {
                items.add(line);
                line = reader.readLine();
            }
            if (items.isEmpty()) {
                logger.log(Level.WARNING, "Inventory file is empty. Using default inventory.");
                setDefaultInventory();
                return;
            }
            addItems(items);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error working with inventory file. Using default inventory. " + e.getMessage().toString());
            setDefaultInventory();
        }
    }

    /**
     * Sets the default inventory values (1 life, 1 bomb, 1 max bomb) and saves them to the "inventory.txt" file.
     */
    public void setDefaultInventory() {
        this.lives = 1;
        this.bombs = 1;
        this.maxBombs = 1;
        saveInventory();
    }

    /**
     * Adds items to the inventory based on a list of item names.
     *
     * @param items the list of item names
     */
    public void addItems(List<String> items) {
        for (String item : items) {
            switch (item) {
                case "Life":
                    this.lives += 1;
                    break;
                case "Bomb":
                    this.maxBombs += 1;
                    this.bombs += 1;
                    break;
                case "Gun":
                    this.addItem(new Gun(true));
                    break;
                default:
                    throw new RuntimeException("Invalid inventory item: " + item);
            }
        }
        logger.log(Level.INFO, "All items added.");
    }

    /**
     * Returns the number of lives in the inventory.
     *
     * @return the number of lives
     */
    public int getLives() {
        return lives;
    }

    /**
     * Sets the number of lives in the inventory.
     *
     * @param lives the number of lives
     */
    public void setLives(int lives) {
        this.lives = lives;
    }

    /**
     * Returns the maximum number of bombs in the inventory.
     *
     * @return the maximum number of bombs
     */
    public int getMaxBombs() {
        return maxBombs;
    }

    /**
     * Sets the maximum number of bombs in the inventory.
     *
     * @param maxBombs the maximum number of bombs
     */
    public void setMaxBombs(int maxBombs) {
        this.maxBombs = maxBombs;
    }

    /**
     * Returns the list of guns in the inventory.
     *
     * @return the list of guns
     */
    public List<Gun> getGuns() {
        return guns;
    }

    /**
     * Saves the inventory data to the "inventory.txt" file.
     */
    public void saveInventory() {
        try {
            File inventoryFile = new File("inventory.txt");
            FileWriter fw = new FileWriter(inventoryFile);
            fw.write(saveBombs());
            fw.write(saveLives());
            fw.write(saveGuns());
            fw.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Error saving inventory. " + e.getMessage().toString());
            throw new RuntimeException(e);
        }
        logger.log(Level.INFO, "Inventory saved.");
    }

    /**
     * Returns a string representation of the bombs in the inventory.
     *
     * @return the string representation of bombs
     */
    public String saveBombs() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < maxBombs; i++) {
            res.append("Bomb\n");
        }
        return res.toString();
    }

    /**
     * Returns a string representation of the lives in the inventory.
     *
     * @return the string representation of lives
     */
    public String saveLives() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < lives; i++) {
            res.append("Life\n");
        }
        return res.toString();
    }

    /**
     * Returns a string representation of the guns in the inventory.
     *
     * @return the string representation of guns
     */
    public String saveGuns() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < guns.size(); i++) {
            res.append("Gun\n");
        }
        return res.toString();
    }

    /**
     * Returns the number of bombs in the inventory.
     *
     * @return the number of bombs
     */
    public int getBombs() {
        return bombs;
    }

    /**
     * Sets the number of bombs in the inventory.
     *
     * @param bombs the number of bombs
     */
    public void setBombs(int bombs) {
        this.bombs = bombs;
    }
}
