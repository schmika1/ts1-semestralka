package cz.fel.cvut.pjv.Exceptions;

public class InvalidMapFormatException extends Exception{
    public InvalidMapFormatException() {
        super("Invalid map format.");
    }
}
