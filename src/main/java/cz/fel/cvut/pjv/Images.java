package cz.fel.cvut.pjv;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * Enumeration of Images used in the game.
 */
public enum Images {
    PLAYER("/p1.png"),
    ENEMY("/enemy.png"),
    BOMB("/bomb.png"),
    FLOOR("/floor.png"),
    WALL("/hardWall.png"),
    BOX("/softWall.png"),
    BOMB_BONUS("/bomb_bonus.png"),
    GUN("/gun.png"),
    LIFE("/heart.png"),
    EXPLOSION("/explosion.png");

    private String path;
    private BufferedImage img;

    /**
     * Constructs an `Images` enum with the specified image path.
     *
     * @param path the path to the image file
     */
    Images(String path){
        this.path = path;
        try {
            this.img = ImageIO.read(Objects.requireNonNull(getClass().getResource(path)));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the BufferedImage associated with the image.
     *
     * @return the BufferedImage associated with the image
     */
    public BufferedImage getImage(){
        return img;
    }

    /**
     * Returns a subImage of the image with the specified dimensions.
     *
     * @param x      the x-coordinate of the upper-left corner of the subImage
     * @param y      the y-coordinate of the upper-left corner of the subImage
     * @param width  the width of the subImage
     * @param height the height of the subImage
     * @return the subImage with the specified dimensions
     */
    public BufferedImage getSubImage(int x, int y, int width, int height){
        return img.getSubimage(x, y, width, height);
    }
}
