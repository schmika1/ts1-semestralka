package cz.fel.cvut.pjv;

/**
 * Represents the different states of the game.
 */
public enum GameStates {
    PLAY,
    GAME_OVER,
    WIN,
    END;
}