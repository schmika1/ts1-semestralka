package cz.fel.cvut.pjv;

import java.io.IOException;
import java.util.logging.*;

/**
 * The GameLogger class provides a singleton logger instance for logging game events.
 */
public final class GameLogger {
    private static Logger gameLogger;
    private FileHandler fh;
    private static GameLogger instance;
    private boolean loggingOn = false;

    // Formatting of log file from: https://www.digitalocean.com/community/tutorials/logger-in-java-logging-example

    /**
     * Private constructor to prevent external instantiation of the GameLogger class.
     */
    private GameLogger() {
        this.gameLogger = Logger.getLogger(this.getClass().getName());

        try {
            fh = new FileHandler("logger.log");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        gameLogger.setUseParentHandlers(false);
        gameLogger.addHandler(fh);
    }

    /**
     * Retrieves the singleton instance of the GameLogger class.
     *
     * @return the GameLogger instance
     */
    public static GameLogger getInstance() {
        if (instance == null) {
            instance = new GameLogger();
        }
        return instance;
    }

    /**
     * Logs a message with the specified log level.
     *
     * @param level the log level
     * @param s     the message to log
     */
    public void log(Level level, String s) {
        if (loggingOn)
            gameLogger.log(new LogRecord(level, s));
    }

    /**
     * Sets the logging state.
     *
     * @param b true to enable logging, false to disable logging
     */
    public void setLogging(boolean b) {
        this.loggingOn = b;
    }
}
