package cz.fel.cvut.pjv;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.logging.Level;

/**
 * Represents a Bomb object that can be placed by a Player. The Bomb kills the Player when it explodes on the tile
 * where the Player was or on the tile next to the bomb and destroys breakable tiles in near distance of 1 tile in each direction.
 */
public class Bomb extends GameObject {
    private static final GameLogger logger = GameLogger.getInstance();
    private int timer;
    private Player author;

    /**
     * Creates a Bomb object.
     *
     * @param isCollectable true if the Bomb object can be collected, otherwise false
     * @param timer         time in seconds between placing and exploding the bomb
     * @param author        the Player who placed the bomb
     */
    public Bomb(boolean isCollectable, int timer, Player author) {
        super(isCollectable);
        this.timer = timer;
        this.author = author;
    }

    /**
     * Checks if the Player is hit by the bomb explosion.
     *
     * @param tiles  the 2D array of tiles representing the game map
     * @param row    the row index of the bomb's location
     * @param col    the column index of the bomb's location
     * @param player the Player object to check for hit
     */
    public void hitPlayer(Tile[][] tiles, int row, int col, Player player) {
        if ((col != tiles[row].length - 1 && player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[row][col + 1])
                || (col != 0 && player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[row][col - 1])
                || (row != tiles.length - 1 && player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[row + 1][col])
                || (row != 0 && player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[row - 1][col])
                || player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[row][col]) {
            if (player.getInventory().getLives() == 1) {
                player.getInventory().setLives(player.getInventory().getLives() - 1);
                logger.log(Level.INFO, "Hit by a bomb. No lives remaining.");
                player.die();
            } else if (player.getInventory().getLives() > 0) {
                player.getInventory().setLives(player.getInventory().getLives() - 1);
                logger.log(Level.INFO, "Hit by a bomb. Remaining lives: " + player.getInventory().getLives());
            }
        }
    }

    /**
     * Explodes the bomb, destroying objects and killing players/enemies in the vicinity.
     *
     * @param tiles   the 2D array of tiles representing the game map
     * @param row     the row index of the bomb's location
     * @param col     the column index of the bomb's location
     * @param player  the Player object
     * @param enemies the list of Enemy objects
     */
    public void explode(Tile[][] tiles, int row, int col, Player player, List<Enemy> enemies) {
        tiles[row][col].setCanBeWalkedThrough(true);

        hitPlayer(tiles, row, col, player);

        for (int i = 0; i < enemies.size(); i++) {
            hitPlayer(tiles, row, col, enemies.get(i));
        }

        if (col != tiles[row].length - 1) {
            if (tiles[row][col + 1].isBreakable() || tiles[row][col + 1].getType().equals(Images.FLOOR)) {
                tiles[row][col + 1].startExplosion("horizontal");
                tiles[row][col + 1].setType(Images.FLOOR);
                if (tiles[row][col + 1].isBreakable()) {
                    tiles[row][col + 1].dropRandomObject();
                }
                tiles[row][col + 1].setBreakable(false);
            }
        }

        if (col != 0) {
            if (tiles[row][col - 1].isBreakable() || tiles[row][col - 1].getType().equals(Images.FLOOR)) {
                tiles[row][col - 1].startExplosion("horizontal");
                tiles[row][col - 1].setType(Images.FLOOR);
                if (tiles[row][col - 1].isBreakable()) {
                    tiles[row][col - 1].dropRandomObject();
                }
                tiles[row][col - 1].setBreakable(false);
            }
        }

        if (row != tiles.length - 1) {
            if (tiles[row + 1][col].isBreakable() || tiles[row + 1][col].getType().equals(Images.FLOOR)) {
                tiles[row + 1][col].startExplosion("vertical");
                tiles[row + 1][col].setType(Images.FLOOR);
                if (tiles[row + 1][col].isBreakable()) {
                    tiles[row + 1][col].dropRandomObject();
                }
                tiles[row + 1][col].setBreakable(false);
            }
        }

        if (row != 0) {
            if (tiles[row - 1][col].isBreakable() || tiles[row - 1][col].getType().equals(Images.FLOOR)) {
                tiles[row - 1][col].startExplosion("vertical");
                tiles[row - 1][col].setType(Images.FLOOR);
                if (tiles[row - 1][col].isBreakable()) {
                    tiles[row - 1][col].dropRandomObject();
                }
                tiles[row - 1][col].setBreakable(false);
            }
        }

        author.getInventory().setBombs(author.getInventory().getBombs() + 1);
    }

    /**
     * Sets the timer for the bomb.
     *
     * @param timer the time between placing and exploding the bomb
     */
    public void setTimer(int timer) {
        this.timer = timer;
    }

    /**
     * Retrieves the timer for the bomb.
     *
     * @return the timer value
     */
    public int getTimer() {
        return timer;
    }

    /**
     * Paints the Bomb object on the screen.
     *
     * @param painter the Painter object used to draw the Bomb
     * @param x       the x-coordinate for drawing the Bomb
     * @param y       the y-coordinate for drawing the Bomb
     */
    @Override
    public void paint(Painter painter, int x, int y) {
        BufferedImage bombImg = Images.BOMB.getImage();
        painter.drawImage(bombImg, x, y);
    }

    /**
     * Retrieves the Player who placed the bomb.
     *
     * @return the Player object who placed the bomb
     */
    public Player getAuthor() {
        return author;
    }
}
