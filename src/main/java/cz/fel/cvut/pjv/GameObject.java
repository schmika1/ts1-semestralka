package cz.fel.cvut.pjv;

/**
 * Represents a game object in the game.
 */
public class GameObject {
    private boolean isCollectable;
    private Painter painter;

    /**
     * Creates a GameObject.
     *
     * @param isCollectable true if the object can be collected, otherwise false
     */
    public GameObject(boolean isCollectable) {
        this.isCollectable = isCollectable;
    }

    /**
     * Checks if the object is collectable.
     *
     * @return true if the object is collectable, otherwise false
     */
    public boolean isCollectable() {
        return this.isCollectable;
    }

    /**
     * Paints the game object on the screen.
     *
     * @param painter the Painter object used to draw the game object
     * @param x       the x-coordinate for drawing the game object
     * @param y       the y-coordinate for drawing the game object
     */
    public void paint(Painter painter, int x, int y) {

    }

    /**
     * Retrieves the type of the game object.
     *
     * @return the type of the game object
     */
    public String getObjectType() {
        return this.getClass().getSimpleName();
    }

    /**
     * Sets whether the object is collectable.
     *
     * @param collectable true if the object should be collectable, otherwise false
     */
    public void setCollectable(boolean collectable) {
        isCollectable = collectable;
    }
}
