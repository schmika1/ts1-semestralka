package cz.fel.cvut.pjv;

import javax.swing.*;
import java.awt.*;

/**
 * A panel that displays the player's inventory.
 */
public class InventoryPanel extends JPanel {
    private Inventory inventory;
    private JLabel livesLabel;
    private JLabel bombsLabel;
    private JLabel gunsLabel;

    /**
     * Constructs an InventoryPanel with the given inventory and height.
     *
     * @param inventory the player's inventory
     * @param height    the height of the panel
     */
    public InventoryPanel(Inventory inventory, int height) {
        this.inventory = inventory;
        this.setPreferredSize(new Dimension(100, height));
        this.setBackground(Color.GRAY);

        livesLabel = new JLabel("Lives: " + inventory.getLives());
        livesLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        bombsLabel = new JLabel("Bombs: " + inventory.getBombs());
        bombsLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        gunsLabel = new JLabel("Guns: " + inventory.getGuns().size());
        gunsLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(livesLabel);
        add(bombsLabel);
        add(gunsLabel);
    }

    /**
     * Updates the inventory panel to reflect the current inventory.
     */
    public void update() {
        livesLabel.setText("Lives: " + inventory.getLives());
        bombsLabel.setText("Bombs: " + inventory.getBombs());
        gunsLabel.setText("Guns: " + inventory.getGuns().size());
    }

    /**
     * Sets the inventory for the panel.
     *
     * @param inventory the new inventory
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int size = 20;
        int spacing = 5;
        int heartCount = inventory.getLives();
        int bombCount = inventory.getBombs();
        int gunCount = inventory.getGuns().size();
        int maxRows = 3;
        int maxColumns = 4;
        int x = 0;
        int y = 50;

        for (int i = 0; i < maxRows; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (heartCount > 0) {
                    if (i == maxRows - 1 && j == maxColumns - 1) {
                        g.drawString("+" + heartCount, x, y + size);
                    } else {
                        g.drawImage(Images.LIFE.getImage(), x, y, size, size, null);
                        heartCount--;
                    }
                }
                x += size + spacing;
            }

            x = 0;
            y += 20;
            if (heartCount == 0) {
                break;
            }
        }

        for (int i = 0; i < maxRows; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (bombCount > 0) {
                    if (i == maxRows - 1 && j == maxColumns - 1) {
                        g.drawString("+" + bombCount, x, y + size);
                    } else {
                        g.drawImage(Images.BOMB_BONUS.getImage(), x, y, size, size, null);
                        bombCount--;
                    }
                }
                x += size + spacing;
            }

            x = 0;
            y += 20;
            if (bombCount == 0) {
                break;
            }
        }

        for (int i = 0; i < maxRows; i++) {
            for (int j = 0; j < maxColumns; j++) {
                if (gunCount > 0) {
                    if (i == maxRows - 1 && j == maxColumns - 1) {
                        g.drawString("+" + gunCount, x, y + size);
                    } else {
                        g.drawImage(Images.GUN.getImage(), x, y, size, size, null);
                        gunCount--;
                    }
                }
                x += size + spacing;
            }

            x = 0;
            y += 20;
            if (gunCount == 0) {
                break;
            }
        }
    }
}
