package cz.fel.cvut.pjv;

import cz.fel.cvut.pjv.Exceptions.InvalidMapFormatException;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Represents a game map with tiles, players, and enemies.
 */
public class Map {

    private Tile[][] tiles;
    private String path;
    private int numOfRows;
    private int numOfCols;
    private Player player;
    private List<Enemy> enemies;
    private static final GameLogger logger = GameLogger.getInstance();

    /**
     * Creates a new Map object with the specified map file path.
     *
     * @param path the path of the map file
     */
    public Map(String path) {
        this.path = path;
        this.numOfRows = getRows().size();
        this.numOfCols = getRows().get(0).length();
        this.tiles = new Tile[numOfRows][numOfCols];
        this.enemies = new ArrayList<>();
    }

    /**
     * Loads the map from the map file.
     *
     * @throws InvalidMapFormatException if the map format is invalid
     */
    public void loadMap() throws InvalidMapFormatException {
        List<String> rows = this.getRows();
        if (validMap(rows)) {
            for (int i = 0; i < numOfRows; i++) {
                String row = rows.get(i);
                for (int j = 0; j < numOfCols; j++) {
                    char c = row.charAt(j);
                    switch (c) {
                        case 'F':
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
                            break;
                        case 'W':
                            tiles[i][j] = new Tile(Images.WALL, false, false, 32, null);
                            break;
                        case 'B':
                            tiles[i][j] = new Tile(Images.BOX, true, false, 32, null);
                            break;
                        case 'H':
                            GameObject heart = new Life(true);
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, heart);
                            break;
                        case 'M':
                            GameObject bombBonus = new BombBonus(true);
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, bombBonus);
                            break;
                        case 'G':
                            GameObject gun = new Gun(true);
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, gun);
                            break;
                        case 'P':
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
                            if (this.player == null) {
                                this.player = new Player(j * tiles[i][j].getTileSize(), i * tiles[i][j].getTileSize() - 15, 4);
                            } else {
                                throw new RuntimeException("Only one player allowed.");
                            }
                            break;
                        case 'E':
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
                            Enemy enemy = new Enemy(j * tiles[i][j].getTileSize(), i * tiles[i][j].getTileSize() - 15, 1,new Inventory(1,1,1));
                            enemies.add(enemy);
                            break;
                        default:
                            logger.log(Level.WARNING, "Invalid tile. Replacing with FLOOR Tile.");
                            tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
                            break;
                    }
                    tiles[i][j].setRow(i);
                    tiles[i][j].setColumn(j);
                }
            }
            if (player == null) {
                throw new RuntimeException("Invalid map format. No player found.");
            }
            if (enemies.size() == 0) {
                throw new RuntimeException("Invalid map format. At least one enemy required.");
            }
        } else {
            throw new InvalidMapFormatException();
        }
    }

    /**
     * Checks if the map format is valid.
     *
     * @param rows the list of rows in the map
     * @return true if the map format is valid, false otherwise
     */
    public boolean validMap(List<String> rows) {
        for (int i = 0; i < numOfRows; i++) {
            if (rows.get(i).length() != numOfCols) {
                logger.log(Level.WARNING, "Invalid map format. All lines must have the same length.");
                return false;
            }
        }
        return true;
    }

    /**
     * Retrieves the rows of the map from the map file.
     *
     * @return the list of rows in the map
     */
    public List<String> getRows() {
        try {
            File mapFile = new File(this.path);
            BufferedReader reader = new BufferedReader(new FileReader(mapFile));
            String line = reader.readLine();
            List<String> rows = new ArrayList<>();
            while (line != null) {
                rows.add(line);
                line = reader.readLine();
            }
            return rows;
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING, "File not found: " + e.getMessage());
            throw new RuntimeException(e.getMessage());
        } catch (IOException e) {
            logger.log(Level.WARNING, "Error loading map: " + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Retrieves the tiles of the map.
     *
     * @return the 2D array of tiles
     */
    public Tile[][] getTiles() {
        return tiles;
    }

    /**
     * Retrieves the player in the map.
     *
     * @return the player object
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Retrieves the list of enemies in the map.
     *
     * @return the list of enemy objects
     */
    public List<Enemy> getEnemies() {
        return enemies;
    }

    /**
     * Restarts the map by clearing the tiles, player, and enemies.
     */
    public void restartMap() {
        this.tiles = new Tile[numOfRows][numOfCols];
        this.player = null;
        this.enemies.clear();
    }

    /**
     * Updates the map based on the given list of explosions and collision checker.
     *
     * @param explosions       the list of explosion points
     * @param collisionChecker the collision checker object
     */
    public void update(List<Point> explosions, CollisionChecker collisionChecker) {
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j].getObjectType().equals("Bomb")) {
                    Bomb b = ((Bomb) tiles[i][j].getObject());
                    if (!(b.getAuthor().getX() + player.width + 6 > j * 32
                            && b.getAuthor().getX() + 6 < (j + 1) * 32
                            && b.getAuthor().getY() + player.height - 3 > i * 32
                            && b.getAuthor().getY() - 2 < (i + 1) * 32)
                            && b.getAuthor().getPlayerTile(b.getAuthor().getX(), b.getAuthor().getY(), tiles) != tiles[i][j]) {
                        tiles[i][j].setCanBeWalkedThrough(false);
                    }
                    b.setTimer(b.getTimer() - 1);
                    explosions.add(new Point(i, j));
                    if (b.getTimer() <= 0) {
                        b.explode(tiles, i, j, player, this.enemies);
                        tiles[i][j].setObject(null);
                        break;
                    }
                }
                if (player.getPlayerTile(player.getX(), player.getY(), tiles) == tiles[i][j] && tiles[i][j].hasObject() && player.getPlayerTile(player.getX(), player.getY(), tiles).getObject().isCollectable()) {
                    player.pickUp(tiles[i][j].getObject());
                    tiles[i][j].setObject(null);
                }
            }
        }
        for (int i = 0; i < this.enemies.size(); i++) {
            this.enemies.get(i).update(tiles, collisionChecker);
            if (!this.enemies.get(i).isAlive) {
                this.enemies.remove(this.enemies.get(i));
            }
        }
    }

    /**
     * Paints the map using the specified painter.
     *
     * @param painter the painter object
     */
    public void paint(Painter painter) {
        int x = 0;
        int y = 0;
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                Tile tile = tiles[i][j];
                tile.paint(painter, x, y);
                x += 32;
            }
            x = 0;
            y += 32;
        }
        for (int i = 0; i < enemies.size(); i++) {
            enemies.get(i).paint(painter);
        }
    }

    /**
     * Retrieves the number of rows in the map.
     *
     * @return the number of rows
     */
    public int getNumOfRows() {
        return numOfRows;
    }

    /**
     * Retrieves the number of columns in the map.
     *
     * @return the number of columns
     */
    public int getNumOfCols() {
        return numOfCols;
    }


}
