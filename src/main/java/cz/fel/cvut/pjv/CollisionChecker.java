package cz.fel.cvut.pjv;


/**
 * Handles collision checking for the game.
 */
public class CollisionChecker {

    private Tile[][] tiles;

    /**
     * Constructs a CollisionChecker with the specified tiles.
     *
     * @param tiles the array of tiles representing the game map
     */
    public CollisionChecker(Tile[][] tiles) {
        this.tiles = tiles;
    }

    /**
     * Checks collision for the player based on the specified movement.
     *
     * @param player the Player object
     * @param xMove  the movement along the x-axis
     * @param yMove  the movement along the y-axis
     * @return true if collision is detected, false otherwise
     */
    public boolean checkCollision(Player player, int xMove, int yMove) {
        // Check if the new position is out of bounds
        int tileSize = tiles[0][0].getTileSize();
        int newX = (player.getX() + xMove);
        int newY = (player.getY() + yMove);
        if (newX < 0 || (newX + player.width - 6) / tileSize >= tiles[0].length ||
                newY < 0 || (newY + player.height - 2) / tileSize >= tiles.length) {
            return false;
        }


        // Check collision with tiles
        boolean rightDown = tiles[(newY + player.height - 2) / tileSize][(newX + player.width - 6) / tileSize].canBeWalkedThrough();
        boolean leftDown = tiles[(newY + player.height - 2) / tileSize][(newX + 6) / tileSize].canBeWalkedThrough();
        boolean rightUpper = tiles[(newY + player.height - 17) / tileSize][(newX + player.width - 6) / tileSize].canBeWalkedThrough();
        boolean leftUpper = tiles[(newY + player.height - 17) / tileSize][(newX + 6) / tileSize].canBeWalkedThrough();

        // Return true if all sides are walkable, false otherwise
        return rightDown && rightUpper && leftUpper && leftDown;

    }


}

