package cz.fel.cvut.pjv;

import java.awt.*;
import java.util.List;
import java.util.logging.Level;

/**
 * Manages the different states of the game.
 */
public class GameStateManager {
    private GameStates gameState;
    private static final GameLogger logger = GameLogger.getInstance();

    /**
     * Initializes the GameStateManager with the default state as "PLAY".
     */
    public GameStateManager() {
        this.gameState = GameStates.PLAY;
    }

    /**
     * Updates the game state based on the player's status and the number of enemies.
     *
     * @param player  the player object
     * @param enemies the list of enemies
     */
    public void update(Player player, List<Enemy> enemies) {
        if (this.isPlayState()) {
            if (!player.isAlive) {
                this.gameOver();
            }

            if (enemies.size() == 0) {
                this.win();
            }
        }
    }

    /**
     * Checks if the game is in the "PLAY" state.
     *
     * @return true if the game is in the "PLAY" state, false otherwise
     */
    public boolean isPlayState() {
        return gameState == GameStates.PLAY;
    }

    /**
     * Checks if the game is in the "GAME_OVER" state.
     *
     * @return true if the game is in the "GAME_OVER" state, false otherwise
     */
    public boolean isGameOverState() {
        return gameState == GameStates.GAME_OVER;
    }

    /**
     * Checks if the game is in the "WIN" state.
     *
     * @return true if the game is in the "WIN" state, false otherwise
     */
    public boolean isWinState() {
        return gameState == GameStates.WIN;
    }

    /**
     * Checks if the game is in the "END" state.
     *
     * @return true if the game is in the "END" state, false otherwise
     */
    public boolean isEndState() {
        return gameState == GameStates.END;
    }

    /**
     * Sets the game state to "PLAY".
     */
    public void play() {
        this.gameState = GameStates.PLAY;
    }

    /**
     * Sets the game state to "GAME_OVER" and logs the state change.
     */
    public void gameOver() {
        logger.log(Level.INFO, "Game was switched to GAME_OVER state.");
        this.gameState = GameStates.GAME_OVER;
    }

    /**
     * Sets the game state to "WIN" and logs the state change.
     */
    public void win() {
        logger.log(Level.INFO, "Game was switched to WIN state.");
        this.gameState = GameStates.WIN;
    }

    /**
     * Sets the game state to "END" and logs the state change.
     */
    public void endGame() {
        logger.log(Level.INFO, "Game was switched to END state.");
        this.gameState = GameStates.END;
    }

    /**
     * Paints the appropriate message on the screen based on the current game state.
     *
     * @param painter the painter object used for drawing
     * @param width   the width of the screen
     * @param height  the height of the screen
     */

    public void paint(Painter painter, int width, int height) {
        Graphics g = painter.getGraphics();
        if (this.isGameOverState()) {

            g.setColor(new Color(0, 0, 0, 0.5f));
            g.fillRect(0, 0, width, height);

            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 48));
            FontMetrics fm = g.getFontMetrics();
            String gameOverText = "Game Over";
            int textWidth = fm.stringWidth(gameOverText);
            int textHeight = fm.getHeight();
            int a = width / 2 - textWidth / 2;
            int b = height / 2 - textHeight / 2;
            g.drawString(gameOverText, a, b);
        }

        if (this.isWinState()) {

            g.setColor(new Color(0, 0, 0, 0.5f));
            g.fillRect(0, 0, width, height);

            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 48));
            FontMetrics fm = g.getFontMetrics();
            String gameOverText = "Game Won";
            int textWidth = fm.stringWidth(gameOverText);
            int textHeight = fm.getHeight();
            int a = width / 2 - textWidth / 2;
            int b = height / 2 - textHeight / 2;
            g.drawString(gameOverText, a, b);
        }
        if (this.isEndState()) {

            g.setColor(new Color(0, 0, 0, 0.5f));
            g.fillRect(0, 0, width, height);

            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 48));
            FontMetrics fm = g.getFontMetrics();
            String gameOverText = "Congratulations";
            int textWidth = fm.stringWidth(gameOverText);
            int textHeight = fm.getHeight();
            int a = width / 2 - textWidth / 2;
            int b = height / 2 - textHeight / 2;
            g.drawString(gameOverText, a, b);
        }
    }
}
