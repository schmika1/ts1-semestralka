package cz.fel.cvut.pjv;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Class that controls the player's (user's) keyboard actions, such as movement, placing bombs, etc.
 */
public class PlayerListener implements KeyListener {
    private Player player;
    private boolean moveUpKeyPressed = false;
    private boolean moveDownKeyPressed = false;
    private boolean moveLeftKeyPressed = false;
    private boolean moveRightKeyPressed = false;
    private boolean placeBombKeyPressed = false;
    private boolean shootKeyPressed = false;
    private boolean enterKeyPressed = false;
    private boolean escapeKeyPressed = false;

    /**
     * Constructs a new PlayerListener object with the associated player.
     *
     * @param player the player to control
     */
    public PlayerListener(Player player) {
        this.player = player;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Not used
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                moveLeftKeyPressed = true;
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                moveRightKeyPressed = true;
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                moveDownKeyPressed = true;
                break;
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                moveUpKeyPressed = true;
                break;
            case KeyEvent.VK_SPACE:
                placeBombKeyPressed = true;
                break;
            case KeyEvent.VK_ENTER:
                enterKeyPressed = true;
                break;
            case KeyEvent.VK_E:
                shootKeyPressed = true;
                break;
            case KeyEvent.VK_ESCAPE:
                escapeKeyPressed = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                moveLeftKeyPressed = false;
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                moveRightKeyPressed = false;
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                moveDownKeyPressed = false;
                break;
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                moveUpKeyPressed = false;
                break;
            case KeyEvent.VK_SPACE:
                placeBombKeyPressed = false;
                break;
            case KeyEvent.VK_ENTER:
                enterKeyPressed = false;
                break;
            case KeyEvent.VK_E:
                shootKeyPressed = false;
                break;
            case KeyEvent.VK_ESCAPE:
                escapeKeyPressed = false;
                break;
        }
    }

    /**
     * Checks if the move up key is pressed.
     *
     * @return true if the move up key is pressed, false otherwise
     */
    public boolean isMoveUpKeyPressed() {
        return moveUpKeyPressed;
    }

    /**
     * Checks if the move down key is pressed.
     *
     * @return true if the move down key is pressed, false otherwise
     */
    public boolean isMoveDownKeyPressed() {
        return moveDownKeyPressed;
    }

    /**
     * Checks if the move left key is pressed.
     *
     * @return true if the move left key is pressed, false otherwise
     */
    public boolean isMoveLeftKeyPressed() {
        return moveLeftKeyPressed;
    }

    /**
     * Checks if the move right key is pressed.
     *
     * @return true if the move right key is pressed, false otherwise
     */
    public boolean isMoveRightKeyPressed() {
        return moveRightKeyPressed;
    }

    /**
     * Checks if the place bomb key is pressed.
     *
     * @return true if the place bomb key is pressed, false otherwise
     */
    public boolean isPlaceBombKeyPressed() {
        return placeBombKeyPressed;
    }

    /**
     * Checks if the enter key is pressed.
     *
     * @return true if the enter key is pressed, false otherwise
     */
    public boolean isEnterKeyPressed() {
        return enterKeyPressed;
    }

    /**
     * Checks if the shoot key is pressed.
     *
     * @return true if the shoot key is pressed, false otherwise
     */
    public boolean isShootKeyPressed() {
        return shootKeyPressed;
    }

    /**
     * Checks if the escape key is pressed.
     *
     * @return true if the escape key is pressed, false otherwise
     */
    public boolean isEscapeKeyPressed() {
        return escapeKeyPressed;
    }
}
