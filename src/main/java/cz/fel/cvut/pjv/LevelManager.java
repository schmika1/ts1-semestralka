package cz.fel.cvut.pjv;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Manages the levels in the game.
 */
public class LevelManager {
    private int levelIdx;
    private List<String> levels;
    private String levelsPath;
    private static final GameLogger logger = GameLogger.getInstance();

    /**
     * Constructs a LevelManager with the specified levels' path.
     *
     * @param levelsPath the path to the levels file
     */
    public LevelManager(String levelsPath) {
        this.levelsPath = levelsPath;
        readLevels();
    }

    /**
     * Reads the levels from the levels file.
     */
    public void readLevels() {
        this.levels = new ArrayList<>();
        try {
            File levelsFile = new File(this.levelsPath);

            if (!levelsFile.exists()) {
                levelsFile.createNewFile();
                logger.log(Level.WARNING, "Level file does not exist. Using default levels instead.");
                addDefaultLevels();
                return;
            }

            BufferedReader reader = new BufferedReader(new FileReader(levelsFile));
            String level = reader.readLine();
            while (level != null) {
                levels.add(level);
                level = reader.readLine();
            }

            if (levels.isEmpty()) {
                logger.log(Level.WARNING, "Level file is empty. Using default levels instead.");
                addDefaultLevels();
                return;
            }

            logger.log(Level.INFO, "Level file was successfully loaded.");
            this.readLevel();

        } catch (Exception e) {
            logger.log(Level.WARNING, "Something went wrong during loading of levels. Using default levels instead.");
            logger.log(Level.WARNING, e.getMessage().toString());
            addDefaultLevels();
            return;
        }
    }

    /**
     * Adds default levels to the levels list.
     */
    public void addDefaultLevels() {
        try {
            File inventory = new File("inventory.txt");
            if (inventory.exists()) {
                FileWriter fw = new FileWriter("inventory.txt");
                fw.write("");
                fw.close();
            }
        } catch (Exception e) {
            throw new RuntimeException("Error working with file inventory.");
        }

        this.levelIdx = 0;
        levels.add("src/main/resources/level_1.txt");
        levels.add("src/main/resources/level_2.txt");
        levels.add("src/main/resources/level_3.txt");
        logger.log(Level.INFO, "Default levels loaded.");
    }

    /**
     * Reads the level to load from the level index file.
     */
    public void readLevel() {
        try {
            File levelToLoad = new File("level_idx.txt");
            if (levelToLoad.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(levelToLoad));
                String levelStr = reader.readLine();
                if (levelStr != null) {
                    this.levelIdx = Integer.parseInt(levelStr);
                }
                if (levelIdx >= levels.size()) {
                    levelIdx = 0;
                }
                reader.close();
            }
        } catch (Exception e) {
            levelIdx = 0;
            logger.log(Level.WARNING, e.getMessage().toString());
        }
        logger.log(Level.INFO, "Level to load set.");
    }

    /**
     * Saves the levels to the levels file.
     */
    public void saveLevels() {
        try {
            File levelFile = new File(this.levelsPath);
            FileWriter fw = new FileWriter(levelFile);
            for (int i = 0; i < this.levels.size(); i++) {
                fw.write(this.levels.get(i) + "\n");
            }
            fw.close();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error saving levels." + e.getMessage().toString());
            throw new RuntimeException("Error saving levels.");
        }
        logger.log(Level.INFO, "Levels saved.");
    }

    /**
     * Saves the current level index to the level index file.
     */
    public void saveLevelToLoad() {
        try {
            File levelToLoad = new File("level_idx.txt");
            FileWriter fw = new FileWriter(levelToLoad);
            fw.write(String.valueOf(levelIdx));
            fw.close();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error saving level index." + e.getMessage().toString());
            throw new RuntimeException("Error saving level position.");
        }
        logger.log(Level.INFO, "Level index saved.");
    }

    /**
     * Returns the path of the current level.
     *
     * @return the path of the current level
     */
    public String getCurrentLevel() {
        return levels.get(levelIdx);
    }

    /**
     * Increases the level index by one.
     */
    public void levelUp() {
        levelIdx++;
    }

    /**
     * Returns the current level index.
     *
     * @return the current level index
     */
    public int getLevelIdx() {
        return levelIdx;
    }

    /**
     * Sets the current level index.
     *
     * @param levelIdx the current level index
     */
    public void setLevelIdx(int levelIdx) {
        this.levelIdx = levelIdx;
    }

    /**
     * Returns the list of levels.
     *
     * @return the list of levels
     */
    public List<String> getLevels() {
        return levels;
    }
}
