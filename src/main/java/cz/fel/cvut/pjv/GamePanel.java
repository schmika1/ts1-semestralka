package cz.fel.cvut.pjv;

import cz.fel.cvut.pjv.Exceptions.InvalidMapFormatException;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * JPanel that contains the entire game and game loop logic.
 */
public class GamePanel extends JPanel implements Runnable {
    private Tile[][] tiles;
    private Player player;
    private CollisionChecker collisionChecker;
    private Thread gameThread;
    private boolean isRunning;
    private Painter painter;
    private PlayerListener playerListener;
    private InventoryPanel inventoryPanel;
    private final int fps = 60;
    private Map map;
    private GameStateManager gameStateManager;
    private int tileSize;
    List<Point> explosions;
    private static final GameLogger logger = GameLogger.getInstance();

    private LevelManager levelManager;
    private boolean enterPressed = false;

    /**
     * Constructs a GamePanel object with the specified path to the levels.
     *
     * @param levelsPath the path to the levels
     * @throws InvalidMapFormatException if the map format is invalid
     */
    public GamePanel(String levelsPath) throws InvalidMapFormatException {
        this.levelManager = new LevelManager(levelsPath);
        this.map = new Map(levelManager.getCurrentLevel());
        this.initGame();
        this.setSize(this.getMap().getNumOfCols() * this.getTileSize() + this.getTileSize(), this.getMap().getNumOfRows() * this.getTileSize() + this.getTileSize());
        setFocusable(true);
        requestFocus();
    }

    /**
     * Starts the game thread.
     */
    public void startGameThread() {
        isRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    /**
     * Initializes the game by loading the map and setting up game objects.
     *
     * @throws InvalidMapFormatException if the map format is invalid
     */
    public void initGame() throws InvalidMapFormatException {
        try {
            map = new Map(levelManager.getCurrentLevel());
            map.loadMap();
            this.tiles = map.getTiles();
            this.tileSize = tiles[0][0].getTileSize();
            this.player = map.getPlayer();
            if (inventoryPanel == null) {
                this.inventoryPanel = new InventoryPanel(player.inventory, this.getHeight());
            } else {
                this.inventoryPanel.setInventory(player.inventory);
            }
            this.collisionChecker = new CollisionChecker(tiles);
            this.playerListener = new PlayerListener(player);
            addKeyListener(playerListener);
            this.explosions = new ArrayList<>();
            if (gameStateManager == null) {
                this.gameStateManager = new GameStateManager();
            } else {
                gameStateManager.play();
            }
        } catch (InvalidMapFormatException e) {
            logger.log(Level.WARNING, "Game initialization failed. " + e.getMessage());
            throw e;
        }
        logger.log(Level.INFO, "Game successfully initialized.");
    }

    /**
     * Restarts the game.
     *
     * @throws InvalidMapFormatException if the map format is invalid
     */
    public void restartGame() throws InvalidMapFormatException {
        explosions.clear();
        logger.log(Level.INFO, "Game restarted.");
        player.inventory.setDefaultInventory();
        map.restartMap();
        this.initGame();
    }

    @Override
    public void run() {
        double drawInterval = 1000000000 / fps;
        double nextDraw = System.nanoTime() + drawInterval;
        while (isRunning) {
            try {
                update();
            } catch (InvalidMapFormatException e) {
                throw new RuntimeException(e);
            }
            repaint();
            if (!isRunning) {
                break;
            }
            try {
                double remainingTime = (nextDraw - System.nanoTime()) / 1000000;
                if (remainingTime < 0) {
                    remainingTime = 0;
                }
                Thread.sleep((long) remainingTime);
                nextDraw += drawInterval;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Updates the game state.
     *
     * @throws InvalidMapFormatException if the map format is invalid
     */
    public void update() throws InvalidMapFormatException {
        if (gameStateManager.isPlayState()) {
            player.update(playerListener, collisionChecker, map.getTiles(), map.getEnemies());
            map.update(explosions, collisionChecker);

            if (playerListener.isEscapeKeyPressed()) {
                player.getInventory().saveInventory();
                levelManager.saveLevels();
                levelManager.saveLevelToLoad();
                System.exit(0);
            }
        }
        if (gameStateManager.isGameOverState()) {
            if (playerListener.isEscapeKeyPressed()) {
                System.exit(0);
            }

            if (playerListener.isEnterKeyPressed()) {
                restartGame();
            }
        }
        if (gameStateManager.isWinState()) {
            if (playerListener.isEscapeKeyPressed()) {
                System.exit(0);
            }

            if (levelManager.getLevelIdx() < levelManager.getLevels().size() - 1) {
                if (playerListener.isEnterKeyPressed()) {
                    explosions.clear();
                    player.inventory.saveInventory();
                    map.restartMap();
                    levelManager.levelUp();
                    initGame();
                }
            } else {
                gameStateManager.endGame();
            }
        }
        if (gameStateManager.isEndState()) {
            logger.log(Level.INFO, "All levels were finished.");
            if (playerListener.isEscapeKeyPressed()) {
                System.exit(0);
            }

            if (playerListener.isEnterKeyPressed()) {
                levelManager.setLevelIdx(0);
                restartGame();
            }
        }

        gameStateManager.update(player, map.getEnemies());
        inventoryPanel.update();
    }

    /**
     * Sets the inventory panel.
     *
     * @param inventoryPanel the inventory panel to set
     */
    public void setInventoryPanel(InventoryPanel inventoryPanel) {
        this.inventoryPanel = inventoryPanel;
    }

    /**
     * Gets the game map.
     *
     * @return the game map
     */
    public Map getMap() {
        return map;
    }

    /**
     * Gets the tile size.
     *
     * @return the tile size
     */
    public int getTileSize() {
        return tileSize;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.painter = new Painter(g);
        map.paint(painter);
        player.paint(painter);
        gameStateManager.paint(painter, this.getWidth(), this.getHeight());
        inventoryPanel.repaint();
    }

    /**
     * Gets the player object.
     *
     * @return the player object
     */
    public Player getPlayer() {
        return this.player;
    }
}
