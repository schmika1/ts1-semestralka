package cz.fel.cvut.pjv;

import java.awt.image.BufferedImage;

/**
 * Represents a bonus item that can be on the map or can be dropped from destroyed blocks.
 */
public class BombBonus extends GameObject {
    /**
     * Creates a BombBonus object.
     *
     * @param isCollectable true if the object can be collected, otherwise false
     */
    public BombBonus(boolean isCollectable) {
        super(isCollectable);
    }

    /**
     * Paints the BombBonus object on the screen.
     *
     * @param painter the Painter object used to draw the BombBonus
     * @param x       the x-coordinate for drawing the BombBonus
     * @param y       the y-coordinate for drawing the BombBonus
     */
    @Override
    public void paint(Painter painter, int x, int y) {
        BufferedImage bombBonusImg = Images.BOMB_BONUS.getImage();
        painter.drawImage(bombBonusImg, x, y);
    }
}
