package cz.fel.cvut.pjv;

import java.awt.image.BufferedImage;

/**
 * Represents a life object in the game.
 */
public class Life extends GameObject {

    /**
     * Creates a new Life object.
     *
     * @param isCollectable true if the object can be collected, false otherwise
     */
    public Life(boolean isCollectable) {
        super(isCollectable);
    }

    /**
     * Paints the Life object on the screen using the provided painter at the specified position.
     *
     * @param painter the painter used to draw the Life object
     * @param x       the x-coordinate of the position to paint the Life object
     * @param y       the y-coordinate of the position to paint the Life object
     */
    @Override
    public void paint(Painter painter, int x, int y) {
        BufferedImage heartImg = Images.LIFE.getImage();
        painter.drawImage(heartImg, x, y);
    }
}
