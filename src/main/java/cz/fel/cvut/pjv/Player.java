package cz.fel.cvut.pjv;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.logging.Level;

/**
 * Class representing a player.
 */
public class Player {
    protected int x;
    protected int y;
    private static final GameLogger logger = GameLogger.getInstance();
    protected int speed;
    protected Painter painter;
    protected boolean isAlive = true;
    protected Inventory inventory;
    public int width;
    public int height;
    protected String lastMove;
    private boolean readyToShoot = true;

    /**
     * Constructs a player object with the specified initial position and speed.
     *
     * @param x     the initial x-coordinate of the player
     * @param y     the initial y-coordinate of the player
     * @param speed the speed of the player
     */
    public Player(int x, int y, int speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.inventory = new Inventory();
        this.width = Images.PLAYER.getImage().getWidth();
        this.height = Images.PLAYER.getImage().getHeight();
    }

    /**
     * Constructs a player object with the specified initial position and inventory.Used for enemies.
     *
     * @param x         the initial x-coordinate of the player
     * @param y         the initial y-coordinate of the player
     * @param inventory the inventory of the player
     */
    public Player(int x, int y, int speed,Inventory inventory) {
        this.x = x;
        this.y = y;
        this.speed =speed;
        this.inventory = inventory;
    }

    /**
     * Makes the player die. The player disappears and loses all items from the inventory.
     * The player's state is set to not alive.
     */
    public void die() {
        logger.log(Level.INFO, "Player died.");
        this.isAlive = false;
    }

    /**
     * Updates the player based on the player input, collision checking, and game state.
     *
     * @param playerListener   the player listener object for input handling
     * @param collisionChecker the collision checker object for checking collisions
     * @param tiles            the 2D array of tiles representing the game map
     * @param enemies          the list of enemy objects in the game
     */
    public void update(PlayerListener playerListener, CollisionChecker collisionChecker, Tile[][] tiles, List<Enemy> enemies) {
        if (playerListener.isMoveUpKeyPressed()) {
            lastMove = "UP";
            if (collisionChecker.checkCollision(this, 0, -this.getSpeed())) {
                this.moveUp();
            }
        }

        if (playerListener.isMoveDownKeyPressed()) {
            lastMove = "DOWN";
            if (collisionChecker.checkCollision(this, 0, this.getSpeed())) {
                this.moveDown();
            }
        }

        if (playerListener.isMoveLeftKeyPressed()) {
            lastMove = "LEFT";
            if (collisionChecker.checkCollision(this, -this.getSpeed(), 0)) {
                this.moveLeft();
            }
        }

        if (playerListener.isMoveRightKeyPressed()) {
            lastMove = "RIGHT";
            if (collisionChecker.checkCollision(this, this.getSpeed(), 0)) {
                this.moveRight();
            }
        }

        if (playerListener.isPlaceBombKeyPressed()) {
            if (this.getInventory().getBombs() != 0) {
                Tile tileToPlaceBomb = this.getPlayerTile(this.getX(), this.getY(), tiles);
                this.placeBomb(tileToPlaceBomb, tiles);
            } else {
                logger.log(Level.INFO, "Placing bomb failed. No free bombs in inventory.");
            }
        }

        if (this.inventory.getGuns().size() != 0) {
            Gun gun = this.inventory.getGuns().get(0);
            gun.setCollectable(false);

            if (playerListener.isShootKeyPressed()) {
                if (readyToShoot) {
                    gun.shoot(tiles, this.lastMove, (this.y + this.height - 2) / 32, (this.x + 6) / 32, enemies);
                    logger.log(Level.INFO, "Player shot in direction: " + lastMove);
                    inventory.getGuns().remove(gun);
                    readyToShoot = false;
                }
            } else {
                readyToShoot = true;
            }
        }
    }

    /**
     * Retrieves the tile object at the player's current position on the map.
     *
     * @param x     the x-coordinate of the player
     * @param y     the y-coordinate of the player
     * @param tiles the 2D array of tiles representing the game map
     * @return the tile object at the player's position
     */
    public Tile getPlayerTile(int x, int y, Tile[][] tiles) {
        return tiles[(y + this.height - 2) / 32][(x + this.width - 6) / 32];
    }

    /**
     * Picks up a game object and adds it to the player's inventory.
     *
     * @param go the game object to pick up
     */
    public void pickUp(GameObject go) {
        inventory.addItem(go);
        logger.log(Level.INFO, "Player picked up object: " + go.getObjectType());
    }

    /**
     * Moves the player up by its speed.
     */
    public void moveUp() {
        this.setY(this.getY() - speed);
    }

    /**
     * Moves the player down by its speed.
     */
    public void moveDown() {
        this.setY(this.getY() + speed);
    }

    /**
     * Moves the player left by its speed.
     */
    public void moveLeft() {
        this.setX(this.getX() - speed);
    }

    /**
     * Moves the player right by its speed.
     */
    public void moveRight() {
        this.setX(this.getX() + speed);
    }

    /**
     * Places a bomb at the specified tile if it is empty.
     *
     * @param tile  the tile to place the bomb
     * @param tiles the 2D array of tiles representing the game map
     */
    public void placeBomb(Tile tile, Tile[][] tiles) {
        if (!tile.hasObject()) {
            Bomb bomb = new Bomb(false, 2 * 60, this);
            tile.setObject(bomb);
            inventory.setBombs(inventory.getBombs() - 1);
        }
    }

    /**
     * Retrieves the x-coordinate of the player.
     *
     * @return the x-coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Retrieves the y-coordinate of the player.
     *
     * @return the y-coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the x-coordinate of the player.
     *
     * @param x the x-coordinate
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Sets the y-coordinate of the player.
     *
     * @param y the y-coordinate
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Paints the player and its equipped gun on the game canvas using the specified painter.
     *
     * @param painter the painter object
     */
    public void paint(Painter painter) {
        BufferedImage playerImg = Images.PLAYER.getImage();
        painter.drawImage(playerImg, this.getX(), this.getY());
        List<Gun> guns = this.inventory.getGuns();
        if (guns.size() != 0) {
            guns.get(0).paintPlayersGun(painter, this.x, this.y - this.height / 2, guns.size());
        }
    }

    /**
     * Retrieves the speed of the player.
     *
     * @return the player speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Retrieves the inventory of the player.
     *
     * @return the player's inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Checks if the player is alive.
     *
     * @return true if the player is alive, false otherwise
     */
    public boolean isAlive() {
        return isAlive;
    }
}
