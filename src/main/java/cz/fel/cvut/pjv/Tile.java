package cz.fel.cvut.pjv;

import java.util.Random;
import java.util.logging.Level;

/**
 * Represents one tile on the map.
 */
public class Tile {
    private Images type;
    private boolean isBreakable;
    private Painter painter;
    private boolean canBeWalkedThrough;
    private GameObject object;
    private int row;
    private int column;

    private int tileSize;
    private boolean shot = false;
    private ShotThread shotThread;
    private String explosionDirection;
    private static final GameLogger logger = GameLogger.getInstance();
    private Random random;

    private class ShotThread extends Thread {
        private static final long SHOT_DURATION = 250;

        @Override
        public void run() {
            try {
                Thread.sleep(SHOT_DURATION);
            } catch (InterruptedException e) {
                logger.log(Level.WARNING, "ShotThread Error" + e.getMessage().toString());
            }
            shot = false;
        }
    }

    /**
     * Constructor of one tile of the map.
     *
     * @param type               The type of the tile, an enum Image containing the image of the tile.
     * @param isBreakable        True when the tile can be destroyed by a bomb, otherwise false.
     * @param canBeWalkedThrough True when the tile can be walked through by players, otherwise false.
     * @param tileSize           The size of the tile in pixels.
     * @param object             The game object associated with the tile.
     */
    public Tile(Images type, boolean isBreakable, boolean canBeWalkedThrough, int tileSize, GameObject object) {
        this.type = type;
        this.isBreakable = isBreakable;
        this.canBeWalkedThrough = canBeWalkedThrough;
        this.tileSize = 32;
        this.object = object;
        this.random = new Random();
    }

    /**
     * Starts the explosion animation on the tile.
     *
     * @param direction The direction of the explosion (vertical or horizontal).
     */
    public void startExplosion(String direction) {
        shot = true;
        this.explosionDirection = direction;
        shotThread = new ShotThread();
        shotThread.start();
    }

    /**
     * Returns the type of the tile.
     *
     * @return The type of the tile.
     */
    public Images getType() {
        return type;
    }

    /**
     * Checks if the tile is breakable.
     *
     * @return True if the tile is breakable, false otherwise.
     */
    public boolean isBreakable() {
        return isBreakable;
    }

    /**
     * Paints the tile on the screen.
     *
     * @param painter The painter object used to draw the tile.
     * @param x       The x-coordinate of the tile's position.
     * @param y       The y-coordinate of the tile's position.
     */
    public void paint(Painter painter, int x, int y) {
        painter.drawImage(this.getType().getImage(), x, y);
        if (shot) {
            if (explosionDirection.equals("vertical")) {
                painter.drawImage(Images.EXPLOSION.getSubImage(5 * 32, 2 * 32, 32, 32), x, y);
            } else if (explosionDirection.equals("horizontal")) {
                painter.drawImage(Images.EXPLOSION.getSubImage(5 * 32, 32, 32, 32), x, y);
            }
        }
        if (this.hasObject()) {
            this.object.paint(painter, x, y);
        }
    }

    /**
     * Drops a random object on the tile.
     */
    public void dropRandomObject() {
        int dropProbability = random.nextInt(100);
        if (!this.hasObject()) {
            if (dropProbability < 2) {
                Gun gun = new Gun(true);
                this.setObject(gun);
            } else if (dropProbability > 2 && dropProbability <= 5) {
                BombBonus bombBonus = new BombBonus(true);
                this.setObject(bombBonus);
            } else if (dropProbability > 5 && dropProbability <= 8) {
                Life heart = new Life(true);
                this.setObject(heart);
            } else {
                this.setObject(null);
            }
        }
    }

    /**
     * Returns the size of the tile.
     *
     * @return The size of the tile in pixels.
     */
    public int getTileSize() {
        return tileSize;
    }

    /**
     * Checks if the tile can be walked through.
     *
     * @return True if the tile can be walked through, false otherwise.
     */
    public boolean canBeWalkedThrough() {
        return canBeWalkedThrough;
    }

    /**
     * Sets the type of the tile.
     *
     * @param type The new type of the tile.
     */
    public void setType(Images type) {
        if (type == Images.FLOOR) {
            this.canBeWalkedThrough = true;
        }
        this.type = type;
    }

    /**
     * Returns the type of the game object associated with the tile.
     *
     * @return The type of the game object as a string, or "No object here." if there is no object.
     */
    public String getObjectType() {
        if (hasObject()) {
            return object.getClass().getSimpleName();
        }
        return "No object here.";
    }

    /**
     * Checks if the tile has a game object associated with it.
     *
     * @return True if the tile has a game object, false otherwise.
     */
    public boolean hasObject() {
        return object != null;
    }

    /**
     * Checks if the tile has a bomb as its game object.
     *
     * @return True if the tile has a bomb, false otherwise.
     */
    public boolean hasBomb() {
        return getObjectType().equals("Bomb");
    }

    /**
     * Sets the game object associated with the tile.
     *
     * @param object The new game object associated with the tile.
     */
    public void setObject(GameObject object) {
        this.object = object;
    }

    /**
     * Returns the game object associated with the tile.
     *
     * @return The game object associated with the tile, or null if there is no object.
     */
    public GameObject getObject() {
        return this.object;
    }

    /**
     * Sets whether the tile can be walked through.
     *
     * @param canBeWalkedThrough True if the tile can be walked through, false otherwise.
     */
    public void setCanBeWalkedThrough(boolean canBeWalkedThrough) {
        this.canBeWalkedThrough = canBeWalkedThrough;
    }

    /**
     * Sets whether the tile is breakable.
     *
     * @param breakable True if the tile is breakable, false otherwise.
     */
    public void setBreakable(boolean breakable) {
        isBreakable = breakable;
    }

    /**
     * Sets the row index of the tile.
     *
     * @param row The row index of the tile.
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Sets the column index of the tile.
     *
     * @param column The column index of the tile.
     */
    public void setColumn(int column) {
        this.column = column;
    }

    /**
     * Returns the row index of the tile.
     *
     * @return The row index of the tile.
     */
    public int getRow() {
        return row;
    }

    /**
     * Returns the column index of the tile.
     *
     * @return The column index of the tile.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Sets whether the tile is currently being shot.
     *
     * @param shot True if the tile is being shot, false otherwise.
     */
    public void setShot(boolean shot) {
        this.shot = shot;
    }

    /**
     * Checks if the tile is currently being shot.
     *
     * @return True if the tile is being shot, false otherwise.
     */
    public boolean isShot() {
        return shot;
    }
}
