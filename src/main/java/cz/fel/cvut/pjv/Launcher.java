package cz.fel.cvut.pjv;

import cz.fel.cvut.pjv.Exceptions.InvalidMapFormatException;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * Class that launches the game and creates the basic game panel.
 */
public class Launcher extends JFrame {
    int height;
    int width;
    /**
     * Constructs a new instance of the Launcher class with the specified title.
     *
     * @param title The title of the game.
     */
    public Launcher(String title) {
        try {
            GamePanel gamePanel = new GamePanel("levels.txt");

            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.height = gamePanel.getMap().getNumOfRows() * gamePanel.getTileSize() + gamePanel.getTileSize();
            this.width = gamePanel.getMap().getNumOfCols() * gamePanel.getTileSize() + gamePanel.getTileSize() / 2;
            InventoryPanel inventoryPanel = new InventoryPanel(gamePanel.getPlayer().getInventory(), height);
            gamePanel.setInventoryPanel(inventoryPanel);
            gamePanel.startGameThread();
            this.setSize(width + inventoryPanel.getPreferredSize().width, height);

            add(gamePanel, BorderLayout.CENTER);
            add(inventoryPanel, BorderLayout.EAST);

            this.setTitle(title);
            this.setVisible(true);

        } catch (InvalidMapFormatException e) {
            e.printStackTrace();
            String errorMessage = "Error loading map: " + e.getMessage();
            JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
    }

    /**
     * Main method to launch the game.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            if (Objects.equals(args[0], "LOG")) {
                GameLogger.getInstance().setLogging(true);
            }
        }
        SwingUtilities.invokeLater(() -> new Launcher("Bomb Game"));
    }
}
