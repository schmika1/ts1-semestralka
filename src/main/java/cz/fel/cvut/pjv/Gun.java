package cz.fel.cvut.pjv;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.logging.Level;

/**
 * The Gun class represents a gun object in the game.
 * It extends the GameObject class.
 */
public class Gun extends GameObject {

    private boolean shot = false;
    private static final GameLogger logger = GameLogger.getInstance();

    /**
     * Constructs a Gun object.
     *
     * @param isCollectable indicates if the gun is collectable
     */
    public Gun(boolean isCollectable) {
        super(isCollectable);
    }

    /**
     * Shoots the enemy if it is located on the given tile.
     *
     * @param enemies     the list of enemies in the game
     * @param hittedTile  the tile that was hit by the gun
     * @param tiles       the array of tiles in the game
     */
    public void shootEnemy(List<Enemy> enemies, Tile hittedTile, Tile[][] tiles) {
        for (int i = 0; i < enemies.size(); i++) {
            Enemy enemy = enemies.get(i);
            if (enemy.getPlayerTile(enemy.getX(), enemy.getY(), tiles).equals(hittedTile)) {
                logger.log(Level.INFO, "Enemy was shot and killed with a gun.");
                enemy.die();
            }
        }
    }

    /**
     * Shoots the gun in the specified direction, destroying tiles and killing enemies if hit.
     *
     * @param tiles   the array of tiles in the game
     * @param direction the shooting direction ("UP", "DOWN", "LEFT", "RIGHT")
     * @param row     the row index of the starting position
     * @param col     the column index of the starting position
     * @param enemies the list of enemies in the game
     */
    public void shoot(Tile[][] tiles, String direction, int row, int col, List<Enemy> enemies) {
        switch (direction) {
            case "UP":
                while (row != 0 && (tiles[row - 1][col].isBreakable() || tiles[row - 1][col].getType().equals(Images.FLOOR))) {
                    tiles[row - 1][col].startExplosion("vertical");
                    shootEnemy(enemies, tiles[row - 1][col], tiles);
                    tiles[row - 1][col].setType(Images.FLOOR);
                    if (tiles[row - 1][col].isBreakable()) {
                        tiles[row - 1][col].dropRandomObject();
                        tiles[row - 1][col].setBreakable(false);
                    }
                    row -= 1;
                }
                break;
            case "DOWN":
                while (row != tiles.length - 1 && (tiles[row + 1][col].isBreakable() || tiles[row + 1][col].getType().equals(Images.FLOOR))) {
                    tiles[row + 1][col].startExplosion("vertical");
                    shootEnemy(enemies, tiles[row + 1][col], tiles);
                    tiles[row + 1][col].setType(Images.FLOOR);
                    if (tiles[row + 1][col].isBreakable()) {
                        tiles[row + 1][col].dropRandomObject();
                        tiles[row + 1][col].setBreakable(false);
                    }
                    row += 1;
                }
                break;
            case "LEFT":
                while (col != 0 && (tiles[row][col - 1].isBreakable() || tiles[row][col - 1].getType().equals(Images.FLOOR))) {
                    tiles[row][col - 1].startExplosion("horizontal");
                    shootEnemy(enemies, tiles[row][col - 1], tiles);
                    tiles[row][col - 1].setType(Images.FLOOR);
                    if (tiles[row][col - 1].isBreakable()) {
                        tiles[row][col - 1].dropRandomObject();
                        tiles[row][col - 1].setBreakable(false);
                    }
                    col -= 1;
                }
                break;
            case "RIGHT":
                while (col != tiles[row].length - 1 && (tiles[row][col + 1].isBreakable() || tiles[row][col + 1].getType().equals(Images.FLOOR))) {
                    tiles[row][col + 1].startExplosion("horizontal");
                    shootEnemy(enemies, tiles[row][col + 1], tiles);
                    tiles[row][col + 1].setType(Images.FLOOR);
                    if (tiles[row][col + 1].isBreakable()) {
                        tiles[row][col + 1].dropRandomObject();
                        tiles[row][col + 1].setBreakable(false);
                    }
                    col += 1;
                }
                break;
        }

        this.shot = true;
    }

    @Override
    public void paint(Painter painter, int x, int y) {
        BufferedImage gunImg = Images.GUN.getImage();
        if (isCollectable()) {
            painter.drawImage(gunImg, x, y);
        }
    }

    /**
     * Paints the player's gun icon above his head with the number of available guns.
     *
     * @param painter    the painter object to draw on the screen
     * @param x          the x-coordinate of the gun's position
     * @param y          the y-coordinate of the gun's position
     * @param numOfGuns  the number of available guns
     */
    public void paintPlayersGun(Painter painter, int x, int y, int numOfGuns) {
        BufferedImage gunImg = Images.GUN.getImage();
        int scaledWidth = 20;
        int scaledHeight = 20;
        BufferedImage scaledGunImg = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = scaledGunImg.createGraphics();
        g2d.drawImage(gunImg, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
        painter.drawImage(scaledGunImg, x, y);

        String number = numOfGuns + "x";
        int numberX = x + scaledWidth + 3;
        int numberY = y + scaledHeight / 2;
        painter.drawString(number, numberX, numberY);
    }
}
