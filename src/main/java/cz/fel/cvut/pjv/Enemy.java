package cz.fel.cvut.pjv;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

/**
 * Represents an enemy in the game.
 */
public class Enemy extends Player {
    private Random random;
    private int movement;
    private boolean collisionFlag = false;
    private static final GameLogger logger = GameLogger.getInstance();

    /**
     * Constructs an Enemy object with the specified position and speed.
     *
     * @param x     the row of the enemy's position
     * @param y     the column of the enemy's position
     * @param speed the speed of the enemy
     */
    public Enemy(int x, int y, int speed, Inventory inventory) {
        super(x, y, speed, inventory);
        this.width = Images.ENEMY.getImage().getWidth();
        this.height = Images.ENEMY.getImage().getHeight();
        this.random = new Random();
        this.movement = random.nextInt(2);

    }

    /**
     * Updates the enemy's movement and actions based on the game state.
     *
     * @param tiles            the array of tiles representing the game map
     * @param collisionChecker the CollisionChecker object for collision detection
     */
    public void update(Tile[][] tiles, CollisionChecker collisionChecker) {
        int currentRow = getPlayerTile(x, y, tiles).getRow();
        int currentCol = getPlayerTile(x, y, tiles).getColumn();

        // Movement behavior for movement type 0 (left-right)
        if (movement == 0) {
            // Check if there's no safe place to go and change direction
            if ((currentCol < tiles[currentRow].length - 1 && !tiles[currentRow][currentCol + 1].canBeWalkedThrough())
                    && (currentCol != 0 && !tiles[currentRow][currentCol - 1].canBeWalkedThrough())) {
                logger.log(Level.INFO, "No place to safely go. Enemy changed direction");
                movement = 1;
            }
            // Move left if collision is detected and there's no bomb in the way
            else if (collisionChecker.checkCollision(this, -this.getSpeed(), 0) && !collisionFlag) {
                if (currentCol > 1 && !tiles[currentRow][currentCol - 2].hasBomb()
                        || (currentCol == 1 && !tiles[currentRow][currentCol - 1].hasBomb())
                        || (currentCol == 0)) {
                    this.moveLeft();
                    lastMove = "LEFT";
                } else {
                    collisionFlag = !collisionFlag;
                }
            }
            // Move right if collision is detected and there's no bomb in the way
            else if (collisionChecker.checkCollision(this, this.getSpeed(), 0) && collisionFlag) {
                if (currentCol < tiles[currentRow].length - 2 && !tiles[currentRow][currentCol + 2].hasBomb()
                        || (currentCol == tiles[currentRow].length - 2 && !tiles[currentRow][currentCol + 1].hasBomb())
                        || (currentCol == tiles[currentRow].length - 1)) {
                    this.moveRight();
                    lastMove = "RIGHT";
                } else {
                    collisionFlag = !collisionFlag;
                }
            } else {
                collisionFlag = !collisionFlag;
            }
        }

        // Movement behavior for movement type 1 (up-down)
        if (movement == 1) {
            // Check if there's no safe place to go and change direction
            if ((currentRow < tiles.length - 1 && !tiles[currentRow + 1][currentCol].canBeWalkedThrough())
                    && (currentRow != 0 && !tiles[currentRow - 1][currentCol].canBeWalkedThrough())) {
                logger.log(Level.INFO, "No place to safely go. Enemy changed direction");
                movement = 0;
            }
            // Move down if collision is detected and there's no bomb in the way
            else if (collisionChecker.checkCollision(this, 0, this.getSpeed()) && !collisionFlag) {
                if (currentRow < tiles.length - 2 && !tiles[currentRow + 2][currentCol].hasBomb()
                        || (currentRow == tiles.length - 2 && !tiles[currentRow + 1][currentCol].hasBomb())
                        || (currentRow == tiles.length - 1)) {
                    this.moveDown();
                    lastMove = "DOWN";
                } else {
                    collisionFlag = !collisionFlag;
                }
            }
            // Move up if collision is detected and there's no bomb in the way
            else if (collisionChecker.checkCollision(this, 0, -this.getSpeed()) && collisionFlag) {
                if (currentRow > 1 && !tiles[currentRow - 2][currentCol].hasBomb()
                        || (currentRow == 1 && !tiles[currentRow - 1][currentCol].hasBomb())
                        || (currentRow == 0)) {
                    this.moveUp();
                    lastMove = "UP";
                } else {
                    collisionFlag = !collisionFlag;
                }
            } else {
                collisionFlag = !collisionFlag;
            }
        }

        // Place a bomb randomly based on the last movement direction
        //Some basic check if it doesn't block his way to place a bomb
        int randomPlaceBomb = random.nextInt(100);
        if (randomPlaceBomb == 0) {
            if (this.getInventory().getBombs() != 0) {
                switch (lastMove) {
                    case "RIGHT":
                        if ((currentCol == tiles[currentRow].length - 1)
                                ||currentCol != tiles[currentRow].length - 2 && tiles[currentRow][currentCol + 2].canBeWalkedThrough()
                                || (currentCol == tiles[currentRow].length - 2 && !tiles[currentRow][currentCol + 1].canBeWalkedThrough())) {
                            this.placeBomb(this.getPlayerTile(this.x, this.y, tiles), tiles);
                        }
                        break;
                    case "LEFT":
                        if ((currentCol == 0)
                                ||currentCol != 1 && tiles[currentRow][currentCol - 2].canBeWalkedThrough()
                                || (currentCol == 1 && !tiles[currentRow][currentCol - 1].canBeWalkedThrough())) {
                            this.placeBomb(this.getPlayerTile(this.x, this.y, tiles), tiles);
                        }
                        break;
                    case "UP":
                        if ((currentRow == 0)
                                || currentRow != 1 && tiles[currentRow - 2][currentCol].canBeWalkedThrough()
                                || (currentRow == 1 && !tiles[currentRow - 1][currentCol].canBeWalkedThrough())) {
                            this.placeBomb(this.getPlayerTile(this.x, this.y, tiles), tiles);
                        }
                        break;
                    case "DOWN":
                        if ((currentRow == tiles.length - 1) ||
                                (currentRow != tiles.length - 2 && tiles[currentRow + 2][currentCol].canBeWalkedThrough())
                                || (currentRow == tiles.length - 2 && !tiles[currentRow + 1][currentCol].canBeWalkedThrough())
                        ) {
                            this.placeBomb(this.getPlayerTile(this.x, this.y, tiles), tiles);
                        }
                        break;
                }
            }
        }
    }


    /**
     * Paints the enemy on the screen.
     *
     * @param painter the Painter object used for drawing
     */
    @Override
    public void paint(Painter painter) {
        BufferedImage enemyImg = Images.ENEMY.getImage();
        painter.drawImage(enemyImg, this.getX(), this.getY());
    }
}
