import cz.fel.cvut.pjv.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


class BombTest {
    private Bomb bomb;
    private Tile[][] tiles;
    private Player player;
    private Player author;

    private List<Enemy> enemies;

    @BeforeEach
    void setUp() {
        author = mock(Player.class);

        bomb = new Bomb(false,5,author);
        tiles = new Tile[10][10];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
            }
        }
        player = mock(Player.class);
        enemies = new ArrayList<>();
    }

    @Test
    void testHitPlayer_PlayerHitWithNoLives_RemovesLifeAndKillsPlayer() {
        // Arrange
        int row = 5;
        int col = 5;
        tiles[row][col] = new Tile(Images.FLOOR, false, true, 32, null);

        when(player.getPlayerTile(anyInt(), anyInt(), any())).thenReturn(tiles[row][col]);
        when(player.getInventory()).thenReturn(mock(Inventory.class));
        when(player.getInventory().getLives()).thenReturn(1);

        // Act
        bomb.hitPlayer(tiles, row, col, player);

        // Assert
        verify(player.getInventory(), times(1)).setLives(0);
        verify(player, times(1)).die();
    }

    @Test
    void testHitPlayer_PlayerHitWithRemainingLives_DecreasesLifeCount() {
        // Arrange
        int row = 5;
        int col = 5;
        tiles[row][col] = new Tile(Images.FLOOR, false, true, 32, null);

        when(player.getPlayerTile(anyInt(), anyInt(), any())).thenReturn(tiles[row][col]);
        when(player.getInventory()).thenReturn(mock(Inventory.class));
        when(player.getInventory().getLives()).thenReturn(3);

        // Act
        bomb.hitPlayer(tiles, row, col, player);

        // Assert
        verify(player.getInventory(), times(1)).setLives(2);
        verify(player, never()).die();
    }

    @Test
    void testExplode_DestroysBreakableTilesInVicinity() {
        // Arrange
        int row = 5;
        int col = 5;
        tiles[row][col] = new Tile(Images.FLOOR, false, true, 32, this.bomb);
        tiles[row][col + 1] = new Tile(Images.BOX, true, true, 32, null);
        tiles[row][col - 1] = new Tile(Images.WALL, false, false, 32, null);
        tiles[row + 1][col] = new Tile(Images.BOX, true, true, 32, null);
        tiles[row - 1][col] = new Tile(Images.BOX, true, true, 32, null);
        when(author.getInventory()).thenReturn(mock(Inventory.class));

        // Act
        bomb.explode(tiles, row, col, author, enemies);

        // Assert
        Assertions.assertEquals(Images.FLOOR, tiles[row][col + 1].getType());
        Assertions.assertEquals(Images.WALL, tiles[row][col - 1].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[row + 1][col].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[row - 1][col].getType());
        Assertions.assertFalse(tiles[row][col + 1].isBreakable());
        Assertions.assertFalse(tiles[row][col - 1].isBreakable());
        Assertions.assertFalse(tiles[row + 1][col].isBreakable());
        Assertions.assertFalse(tiles[row - 1][col].isBreakable());
        Assertions.assertTrue(tiles[row][col + 1].canBeWalkedThrough());
        Assertions.assertFalse(tiles[row][col - 1].canBeWalkedThrough());
        Assertions.assertTrue(tiles[row + 1][col].canBeWalkedThrough());
        Assertions.assertTrue(tiles[row - 1][col].canBeWalkedThrough());

    }

    @Test
    void testExplode_dropRandomObjectsFromBreakableTile(){
        // Arrange
        int row = 5;
        int col = 5;
        Tile mockBoxTile = mock(Tile.class);
        Tile mockWallTile = mock(Tile.class);
        Tile mockFloorTile = mock(Tile.class);
        when(mockBoxTile.getType()).thenReturn(Images.BOX);
        when(mockWallTile.getType()).thenReturn(Images.WALL);
        when(mockFloorTile.getType()).thenReturn(Images.FLOOR);
        when(mockBoxTile.isBreakable()).thenReturn(true);
        when(mockWallTile.isBreakable()).thenReturn(false);
        when(mockFloorTile.isBreakable()).thenReturn(false);
        tiles[row][col] = mockFloorTile;
        tiles[row][col + 1] = mockBoxTile;
        tiles[row][col - 1] = mockWallTile;
        tiles[row + 1][col] = mockFloorTile;
        tiles[row - 1][col] = mockBoxTile;
        when(author.getInventory()).thenReturn(mock(Inventory.class));


        // Act
        bomb.explode(tiles, row, col, author, enemies);

        // Assert
        verify(mockBoxTile,times(2)).dropRandomObject();
        verify(mockFloorTile,never()).dropRandomObject();
        verify(mockWallTile,never()).dropRandomObject();

    }


}