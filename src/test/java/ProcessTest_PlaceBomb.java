import cz.fel.cvut.pjv.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ProcessTest_PlaceBomb {

    private Bomb bomb;
    private Tile[][] tiles;
    private Player player;
    private Player author;
    private List<Enemy> enemies;
    private PlayerListener playerListener;

    @BeforeEach
    void setUp() {
        player = mock(Player.class);
        when(player.getInventory()).thenReturn(mock(Inventory.class));
        tiles = new Tile[10][10];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
            }
        }
        author = new Player(0, 0, 0);
        enemies = new ArrayList<>();
        playerListener = mock(PlayerListener.class);
        when(playerListener.isPlaceBombKeyPressed()).thenReturn(true);
    }


    @Test
    void placeBombWithNoBombs() {
        // Arrange
        Inventory inventory = new Inventory();
        inventory.setBombs(0);
        int row = 5;
        int col = 5;
        when(player.getPlayerTile(anyInt(), anyInt(), eq(tiles))).thenReturn(tiles[row][col]);
        player = spy(new Player(0, 0, 0, inventory));
        //Act
        player.update(playerListener, mock(CollisionChecker.class), tiles, enemies);

        //Assert
        verify(player, never()).placeBomb(any(), eq(tiles));

    }

    @Test
    void placeBomb_breakTile_enemyDie_decreaseLives() {
        // Arrange
        Inventory inventory = new Inventory();
        inventory.setBombs(1);
        inventory.setLives(2);
        int row = 5;
        int col = 5;

        player = spy(new Player(0, 0, 0, inventory));
        doReturn(tiles[row][col]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        tiles[row + 1][col] = new Tile(Images.BOX, true, false, 32, null);
        Enemy enemy = mock(Enemy.class);
        enemies.add(enemy);
        when(enemy.getInventory()).thenReturn(mock(Inventory.class));
        when(enemy.getInventory().getLives()).thenReturn(1);
        Bomb bomb = new Bomb(false, 3, player);
        when(enemy.getPlayerTile(anyInt(), anyInt(), eq(tiles))).thenReturn(tiles[row - 1][col]);


        //Act
        player.update(playerListener, mock(CollisionChecker.class), tiles, enemies);
        bomb.explode(tiles, row, col, player, enemies);

        //Assert
        verify(player, times(1)).placeBomb(any(), eq(tiles));
        verify(enemy, times(1)).die();
        Assertions.assertEquals(1, player.getInventory().getLives());
        Assertions.assertEquals(Images.FLOOR, tiles[row + 1][col].getType());

    }


    @Test
    void placeBomb_enemyDie() {
        // Arrange
        Inventory inventory = new Inventory();
        inventory.setBombs(1);
        inventory.setLives(2);
        int row = 5;
        int col = 5;

        player = spy(new Player(0, 0, 0, inventory));
        doReturn(tiles[row][col]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        tiles[row + 2][col] = new Tile(Images.BOX, true, false, 32, null);
        Enemy enemy = mock(Enemy.class);
        enemies.add(enemy);
        when(enemy.getInventory()).thenReturn(mock(Inventory.class));
        when(enemy.getInventory().getLives()).thenReturn(1);
        Bomb bomb = new Bomb(false, 3, player);
        when(enemy.getPlayerTile(anyInt(), anyInt(), eq(tiles))).thenReturn(tiles[row - 1][col]);


        //Act
        player.update(playerListener, mock(CollisionChecker.class), tiles, enemies);
        doReturn(tiles[row - 2][col]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        bomb.explode(tiles, row, col, player, enemies);

        //Assert
        verify(player, times(1)).placeBomb(any(), eq(tiles));
        verify(enemy, times(1)).die();
        Assertions.assertEquals(2, player.getInventory().getLives());
        Assertions.assertEquals(Images.BOX, tiles[row + 2][col].getType());

    }

    @Test
    void placeBomb_breakTile_decreaseLives() {
        // Arrange
        Inventory inventory = new Inventory();
        inventory.setBombs(1);
        inventory.setLives(2);
        int row = 5;
        int col = 5;

        player = spy(new Player(0, 0, 0, inventory));
        doReturn(tiles[row][col]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        tiles[row + 1][col] = new Tile(Images.BOX, true, false, 32, null);
        Enemy enemy = mock(Enemy.class);
        enemies.add(enemy);
        when(enemy.getInventory()).thenReturn(mock(Inventory.class));
        when(enemy.getInventory().getLives()).thenReturn(1);
        Bomb bomb = new Bomb(false, 3, player);
        when(enemy.getPlayerTile(anyInt(), anyInt(), eq(tiles))).thenReturn(tiles[row - 2][col]);


        //Act
        player.update(playerListener, mock(CollisionChecker.class), tiles, enemies);
        bomb.explode(tiles, row, col, player, enemies);

        //Assert
        verify(player, times(1)).placeBomb(any(), eq(tiles));
        verify(enemy, never()).die();
        Assertions.assertEquals(1, player.getInventory().getLives());
        Assertions.assertEquals(Images.FLOOR, tiles[row + 1][col].getType());

    }

    @Test
    void placeBomb_noEffect() {
        // Arrange
        Inventory inventory = new Inventory();
        inventory.setBombs(1);
        inventory.setLives(2);
        int row = 5;
        int col = 5;

        player = spy(new Player(0, 0, 0, inventory));
        doReturn(tiles[row][col]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        tiles[row + 2][col] = new Tile(Images.BOX, true, false, 32, null);
        Enemy enemy = mock(Enemy.class);
        enemies.add(enemy);
        when(enemy.getInventory()).thenReturn(mock(Inventory.class));
        when(enemy.getInventory().getLives()).thenReturn(1);
        Bomb bomb = new Bomb(false, 3, player);
        when(enemy.getPlayerTile(anyInt(), anyInt(), eq(tiles))).thenReturn(tiles[row - 2][col]);


        //Act
        player.update(playerListener, mock(CollisionChecker.class), tiles, enemies);
        doReturn(tiles[row][col+3]).when(player).getPlayerTile(anyInt(), anyInt(), eq(tiles));
        bomb.explode(tiles, row, col, player, enemies);

        //Assert
        verify(player, times(1)).placeBomb(any(), eq(tiles));
        verify(enemy, never()).die();
        Assertions.assertEquals(2, player.getInventory().getLives());
        Assertions.assertEquals(Images.BOX, tiles[row + 2][col].getType());

    }

}
