import cz.fel.cvut.pjv.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


class PlayerTest {
    private Player player;
    private Tile[][] tiles;

    @BeforeEach
    void setUp() {
        tiles = new Tile[10][10];
        player = new Player(0, 0, 1);
    }

    @Test
    void testMoveUp() {
        // Arrange
        int initialY = player.getY();

        // Act
        player.moveUp();

        // Assert
        Assertions.assertEquals(initialY - player.getSpeed(), player.getY());
    }

    @Test
    void testMoveDown() {
        // Arrange
        int initialY = player.getY();

        // Act
        player.moveDown();

        // Assert
        Assertions.assertEquals(initialY + player.getSpeed(), player.getY());
    }

    @Test
    void testMoveLeft() {
        // Arrange
        int initialX = player.getX();

        // Act
        player.moveLeft();

        // Assert
        Assertions.assertEquals(initialX - player.getSpeed(), player.getX());
    }

    @Test
    void testMoveRight() {
        // Arrange
        int initialX = player.getX();

        // Act
        player.moveRight();

        // Assert
        Assertions.assertEquals(initialX + player.getSpeed(), player.getX());
    }

    @Test
    void testPlaceBomb() {
        // Arrange
        Tile tile = new Tile(Images.FLOOR, false, true, 32, null);
        player.getInventory().setMaxBombs(1);
        player.getInventory().setBombs(1);

        // Act
        player.placeBomb(tile, tiles);

        // Assert
        Assertions.assertTrue(tile.hasObject());
        Assertions.assertEquals(Bomb.class, tile.getObject().getClass());
        Assertions.assertEquals(0, player.getInventory().getBombs());
        Assertions.assertEquals(1, player.getInventory().getMaxBombs());
    }

    @Test
    void testPlaceBomb_ObjectOnTile() {
        // Arrange
        Tile tile = mock(Tile.class);
        when(tile.hasObject()).thenReturn(true);
        player.getInventory().setMaxBombs(1);
        player.getInventory().setBombs(1);

        // Act
        player.placeBomb(tile, tiles);

        // Assert
        Assertions.assertTrue(tile.hasObject());
        Assertions.assertEquals(1, player.getInventory().getBombs());
        Assertions.assertEquals(1, player.getInventory().getMaxBombs());
        verify(tile,never()).setObject(any());
    }

    @Test
    void testPickUpLife() {
        // Arrange
        int numOfLives = player.getInventory().getLives();
        GameObject gameObject = new Life(true);

        // Act
        player.pickUp(gameObject);

        // Assert
        Assertions.assertEquals(numOfLives + 1, player.getInventory().getLives());
    }




}
