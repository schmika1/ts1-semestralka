import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import cz.fel.cvut.pjv.*;

import java.util.ArrayList;
import java.util.List;

class GunTest {
    private Gun gun;
    private Tile[][] tiles;
    private List<Enemy> enemies;

    @BeforeEach
    void setUp() {
        gun = new Gun(true);
        tiles = new Tile[10][10];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                tiles[i][j] = new Tile(Images.FLOOR, false, true, 32, null);
            }
        }
        enemies = new ArrayList<>();
    }

    @Test
    void testShootEnemy() {
        // Arrange
        Enemy enemy1 = new Enemy(0, 0, 1, new Inventory(1, 1, 1));
        Enemy enemy2 = new Enemy(64, 64, 1, new Inventory(1, 1, 1));
        Tile hittedTile = enemy1.getPlayerTile(enemy1.getX(), enemy1.getY(), tiles);
        enemies.add(enemy1);
        enemies.add(enemy2);

        // Act
        gun.shootEnemy(enemies, hittedTile, tiles);

        // Assert
        Assertions.assertFalse(enemy1.isAlive());
        Assertions.assertTrue(enemy2.isAlive());
    }

    @Test
    void testShoot_UpDirection() {
        // Arrange
        tiles[1][1] = new Tile(Images.BOX, true, false, 32, null);
        tiles[2][1] = new Tile(Images.WALL, false, false, 32, null);
        tiles[3][1] = new Tile(Images.FLOOR, false, true, 32, null);
        tiles[4][1] = new Tile(Images.BOX, true, false, 32, null);
        tiles[5][1] = new Tile(Images.FLOOR, false, true, 32, null);
        int initialRow = 6;

        // Act
        gun.shoot(tiles, "UP", initialRow, 1, enemies);

        // Assert
        Assertions.assertEquals(Images.FLOOR, tiles[5][1].getType());
        Assertions.assertEquals(false, tiles[5][1].isBreakable());
        Assertions.assertEquals(true, tiles[5][1].canBeWalkedThrough());

        Assertions.assertEquals(Images.FLOOR, tiles[4][1].getType());
        Assertions.assertFalse(tiles[4][1].isBreakable());
        Assertions.assertTrue(tiles[4][1].canBeWalkedThrough());

        Assertions.assertEquals(Images.FLOOR, tiles[3][1].getType());
        Assertions.assertEquals(Images.WALL, tiles[2][1].getType());
        Assertions.assertFalse(tiles[2][1].isBreakable());
        Assertions.assertFalse(tiles[2][1].canBeWalkedThrough());

        Assertions.assertEquals(Images.BOX, tiles[1][1].getType());


    }

    @Test
    void testShoot_DownDirection() {
        // Arrange
        tiles[8][1] = new Tile(Images.WALL, false, false, 32, null);
        tiles[7][1] = new Tile(Images.FLOOR, false, true, 32, null);
        tiles[6][1] = new Tile(Images.BOX, true, false, 32, null);
        tiles[5][1] = new Tile(Images.FLOOR, false, true, 32, null);
        int initialRow = 4;

        // Act
        gun.shoot(tiles, "DOWN", initialRow, 1, enemies);

        // Assert
        Assertions.assertEquals(Images.FLOOR, tiles[5][1].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[6][1].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[7][1].getType());
        Assertions.assertEquals(Images.WALL, tiles[8][1].getType());
        Assertions.assertTrue(tiles[6][1].canBeWalkedThrough());
        Assertions.assertFalse(tiles[8][1].canBeWalkedThrough());
        Assertions.assertFalse(tiles[6][1].isBreakable());
        Assertions.assertFalse(tiles[8][1].isBreakable());

    }

    @Test
    void testShoot_LeftDirection() {
        // Arrange
        tiles[1][0] = new Tile(Images.FLOOR, false, true, 32, null);
        tiles[1][1] = new Tile(Images.BOX, false, true, 32, null);
        tiles[1][2] = new Tile(Images.WALL, false, true, 32, null);
        tiles[1][3] = new Tile(Images.FLOOR, false, true, 32, null);
        int initialCol = 4;

        // Act
        gun.shoot(tiles, "LEFT", 1, initialCol, enemies);

        // Assert
        Assertions.assertEquals(Images.FLOOR, tiles[1][3].getType());
        Assertions.assertEquals(Images.WALL, tiles[1][2].getType());
        Assertions.assertEquals(Images.BOX, tiles[1][1].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[1][0].getType());


    }

    @Test
    void testShoot_RightDirection() {
        // Arrange
        tiles[1][8] = new Tile(Images.FLOOR, false, true, 32, null);
        tiles[1][7] = new Tile(Images.BOX, true, false, 32, null);
        tiles[1][6] = new Tile(Images.BOX, true, false, 32, null);
        tiles[1][5] = new Tile(Images.FLOOR, false, true, 32, null);
        int initialCol = 4;

        // Act
        gun.shoot(tiles, "RIGHT", 1, initialCol, enemies);

        // Assert
        Assertions.assertEquals(Images.FLOOR, tiles[1][5].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[1][6].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[1][7].getType());
        Assertions.assertEquals(Images.FLOOR, tiles[1][8].getType());
        Assertions.assertFalse(tiles[1][6].isBreakable());
        Assertions.assertTrue(tiles[1][6].canBeWalkedThrough());
        Assertions.assertFalse(tiles[1][7].isBreakable());


    }
}
